create database cinema;

-- drop database cinema;

use cinema;

-- show tables;

-- select * from fil_filme;

create table cat_categoria(
cat_id int primary key auto_increment,
cat_descricao varchar(400));

create table fil_filme(
fil_id int primary key auto_increment,
fil_nome varchar(255) not null, 
fil_duracao varchar(20) not null, 
fil_sinopse varchar(500)  not null,
fil_lancamento varchar(20) not null,
fil_categoria int not null,
fil_imagem varchar(50) not null,
foreign key(fil_categoria) references cat_categoria(cat_id));

create table sal_sala(
sal_id int primary key auto_increment,
sal_nome varchar(20) not null,
sal_tamanhoTela varchar(255) not null,
sal_capacidade varchar(50) not null
); 

create table exb_exibicao(
exb_id int primary key auto_increment,
exb_data varchar(20)  not null,
exb_horario varchar(5) not null,
exb_filme int not null, 
exb_sala int not null,
foreign key(exb_sala) references sal_sala(sal_id),
foreign key(exb_filme) references fil_filme(fil_id)
);