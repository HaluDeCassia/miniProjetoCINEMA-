﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_CadastroExibicao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataSet ds = FilmeDB.SelectAll();
            ddlFilme.DataSource = ds;
            ddlFilme.DataValueField = "fil_id";
            ddlFilme.DataTextField = "fil_nome";
            ddlFilme.DataBind();
            ddlFilme.Items.Insert(0, "Selecionar...");

            DataSet ds1 = SalaDB.SelectAll();
            ddlSala.DataSource = ds1;
            ddlSala.DataValueField = "sal_id";
            ddlSala.DataTextField = "sal_nome";
            ddlSala.DataBind();
            ddlSala.Items.Insert(0, "Selecionar...");


        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Exibicao exb = new Exibicao();
        Filme fil = new Filme();
        Sala sal = new Sala();
        exb.exb_filme = fil;
        exb.exb_sala = sal;
        exb.exb_data = txbData.Text;
        exb.exb_horario = ddlHora.SelectedItem.Text;
        exb.exb_filme.fil_id = Convert.ToInt32(ddlFilme.SelectedValue);
        exb.exb_sala.sal_id= Convert.ToInt32(ddlSala.SelectedValue);

        ExibicaoDB.Insert(exb);
    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {

    }
}