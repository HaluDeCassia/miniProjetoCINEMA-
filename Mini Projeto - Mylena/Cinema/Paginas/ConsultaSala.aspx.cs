﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cinema.Funcoes;

public partial class Paginas_ConsultaSala : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataSet ds = new DataSet();
            ds = SalaDB.SelectAll();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lbl.Text += "<fieldset class='consulta'>" +
                                "<br/><center>Sala: " + dr["sal_nome"] + "<br />" +
                                "Capacidade da Sala: " + dr["sal_capacidade"] + "<br />" +
                                "Tamanho da Tela: " + dr["sal_tamanhoTela"] + "<br /></center>" +
                                "<a href='../Paginas/EditarSala.aspx?sal=" + Funcoes.AESCodifica(Convert.ToString(dr["sal_id"])) + "'>" +
                                    "</br><button type='button' class='Salvar'>" +
                                        "Editar" +
                                    "</button>" +
                                "</a>" +
                            "</fieldset>";
            }
        }
    }
}