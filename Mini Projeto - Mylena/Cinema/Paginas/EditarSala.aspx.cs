﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Cinema.Funcoes;
using System.Data;

public partial class Paginas_EditarSala : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["sal"] != null)
            {
                if (Request.QueryString["sal"] != "")
                {
                    try
                    {
                        string sal1 = Request.QueryString["sal"].ToString().Replace(" ", "+");
                        int n1 = Convert.ToInt32(Funcoes.AESDecodifica(sal1));

                        Sala sal = SalaDB.Select(n1);

                        txbCodigo.Text = Convert.ToString(sal.sal_id);
                        txbNome.Text = sal.sal_nome;
                        txbCapacidade.Text = sal.sal_capacidade;

                        if (sal.sal_tamanhoTela == "Grande")
                            ddlTamanho.SelectedIndex = 1;
                        else if (sal.sal_tamanhoTela == "Normal")
                            ddlTamanho.SelectedIndex = 2;
                    }
                    catch (Exception erro)
                    {
                        Response.Redirect("~/Paginas/ConsultaSala.aspx");
                    }

                }
            }
            else
            {
                Response.Redirect("~/Paginas/ConsultaSala.aspx");
            }
        }
    }
    protected void btnGravar_Click(object sender, EventArgs e)
    {
        Sala sal = new Sala();

        sal.sal_id = Convert.ToInt32(txbCodigo.Text);
        sal.sal_nome = txbNome.Text;
        sal.sal_capacidade = txbCapacidade.Text;
        sal.sal_tamanhoTela = ddlTamanho.SelectedItem.Text;

        switch (SalaDB.Atualizar(sal))
        {
            case 0:
                Response.Redirect("~/Paginas/ConsultaSala.aspx");
                break;
            
        }
    }
    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        return;
    }
}