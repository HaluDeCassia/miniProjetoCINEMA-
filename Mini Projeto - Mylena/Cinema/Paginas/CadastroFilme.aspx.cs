﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_CadastroFilme : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataSet ds = CategoriaDB.SelectAll();
            ddlCategoria.DataSource = ds;
            ddlCategoria.DataValueField = "cat_id";
            ddlCategoria.DataTextField = "cat_descricao";
            ddlCategoria.DataBind();
            ddlCategoria.Items.Insert(0, "Selecionar...");
        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Filme fil = new Filme();
        Categoria cat = new Categoria();
        fil.fil_nome = txbNome.Text;
        fil.fil_duracao = txbDuracao.Text;
        fil.fil_sinopse = txbSinopse.Text;
        fil.fil_lancamento = txbLancamento.Text;
        fil.fil_categoria = cat;
        fil.fil_categoria.cat_id = Convert.ToInt32(ddlCategoria.SelectedValue);

        fil.fil_imagem = Anexar();

        FilmeDB.Insert(fil);
    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        txbNome.Text = "";
        txbLancamento.Text = "";
        txbSinopse.Text = "";
        txbDuracao.Text = "";
    }

    protected string Anexar()
    {
        string dir = Request.PhysicalApplicationPath + "Imagens\\" + txbNome.Text.Replace(' ', '-') + "\\";
        string nomeArquivo = "";
        if (!Directory.Exists(dir))
        {
            //Caso não exista devermos criar
            Directory.CreateDirectory(dir);
        }

        try
        { //pegar o nome do arquivo
            string arq = flpArquivos.PostedFile.FileName;
            //tamanho maximo do upload em kb
            double permitido = 400;
            // Teste para verificar se foi submetido o arquivo
            if (flpArquivos.PostedFile != null)
            {
                //identificamos o tamanho do arquivo
                double tamanho = Convert.ToDouble(flpArquivos.PostedFile.ContentLength) / 1024;
                //verificamos a extensão através dos últimos 4 caracteres
                string extensao = arq.Substring(arq.Length - 4).ToLower();

                // o tamanho acima do permitido
                if (tamanho > permitido)
                {
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>infoTamanho();</script>", false);
                    //tamanho do arquivo não suportado!
                }
                else if ((extensao != ".png" && extensao != ".jpg" && extensao != "jpeg"))
                {
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>infoExtensao();</script>", false);
                    //extensão diferente de doc, docx e pdf: extensão inválida
                }
                else
                {
                    string diretorio = "";
                    nomeArquivo = DateTime.Now.ToString("yyyyMMddHHmmss");
                    //diretorio onde será gravado o arquivo criar o diretório arquivos no mesmo local da aplicação
                    if (extensao.Contains('.'))
                    {
                        nomeArquivo = nomeArquivo + extensao;
                    }
                    else
                    {
                        nomeArquivo = nomeArquivo + "." + extensao;
                    }

                    diretorio = Request.PhysicalApplicationPath + "Imagens\\" + txbNome.Text.Replace(' ', '-') + "\\" + nomeArquivo;
                    // verifica se já existe o nome do arquivo submetido

                    flpArquivos.PostedFile.SaveAs(diretorio);
                }
            }
            else
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>error();</script>", false);
            }

            return nomeArquivo;
        }
        catch (Exception ex)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>error();</script>", false);
            return nomeArquivo = "";
        }
    }
}