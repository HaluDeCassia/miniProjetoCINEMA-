﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditarFilme.aspx.cs" Inherits="Paginas_EditarFilme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <center><fieldset>
        
        <legend>Editar Filmes</legend>
        

    <table>
        <tr>
            <td>
                <asp:Label ID="lblCodigo" runat="server" Text="Codigo: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txbCodigo" runat="server" CssClass="tb" Enabled="false"></asp:TextBox>
            </td>

            <td>
                <asp:Label ID="lblCategoria" runat="server" Text="Categoria: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCategoria" runat="server" Width="170px" Height="30px"></asp:DropDownList>
            </td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNome" runat="server" Text="Nome: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbNome" CssClass="tb" runat="server" placeholder="Digite o nome do filme" ></asp:TextBox>

            </td>

            <td>
                <asp:Label ID="lblSinopse" runat="server" Text="Sinopse: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbSinopse" CssClass="tb" runat="server" placeholder="Digite a sinopse do filme" ></asp:TextBox>

            </td>
            
        </tr>
        <tr>   
            <td>
                <asp:Label ID="lblDuracao" runat="server" Text="Duração: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txbDuracao" CssClass="tb" runat="server" placeholder="Digite a duração do filme" ></asp:TextBox>

            </td>

            <td>
                <asp:Label ID="lblLancamento" runat="server" Text="Ano de Lançamento: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbLancamento" CssClass="tb" runat="server" placeholder="Digite a data de lançamento" ></asp:TextBox>

            </td>
        </tr>
        <tr>   
            
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnSalvar" CssClass="Salvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click"></asp:Button></td>
            <td><asp:Button ID="btnExcluir" CssClass="Limpar" runat="server" Text="Excluir" OnClick="btnExcluir_Click"></asp:Button></td>
            <td></td>
        </tr>
        </table>
        
    </fieldset></center>







</asp:Content>

