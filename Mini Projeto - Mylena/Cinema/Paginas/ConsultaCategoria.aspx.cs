﻿using Cinema.Funcoes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_ConsultaCategoria : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataSet ds = new DataSet();
            ds = CategoriaDB.SelectAll();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lbl.Text += "<fieldset class='consulta'>" +
                                "<br/><center>" + dr["cat_descricao"] + "<br /></center>" +
                                "<a href='../Paginas/EditarCategoria.aspx?cat=" + Funcoes.AESCodifica(Convert.ToString(dr["cat_id"])) + "'>" +
                                    "<br/><button type='button' class='SalvarCategoria'>" +
                                        "Editar" +
                                    "</button>" +
                                "</a>" +
                            "</fieldset>";
            }
        }
    }
}