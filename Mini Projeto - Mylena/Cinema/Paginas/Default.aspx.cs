﻿using Cinema.Funcoes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_ConsultaExibicao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataSet ds = new DataSet();
            ds = ExibicaoDB.SelectAll();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Filme fil = FilmeDB.Select(Convert.ToInt32(dr["exb_filme"]));
                Sala sal = SalaDB.Select(Convert.ToInt32(dr["exb_sala"]));
                lbl.Text += "<fieldset class='exibicao'>" +
                                "<img src='../Imagens/" + fil.fil_nome.Replace(' ', '-') + "/" + fil.fil_imagem + "' />" +
                                "<br/><center>Filme: " + fil.fil_nome + "<br />" +
                                "Categoria: " + fil.fil_categoria.cat_descricao + "<br />" +
                                "Data: " + dr["exb_data"] + "<br />" +
                                "Sala: " + sal.sal_nome + "<br />" +
                                "Horario: " + dr["exb_horario"] + "<br /></center>" +
                                "<a href='../Paginas/EditarExibicao.aspx?exi=" + Funcoes.AESCodifica(Convert.ToString(dr["exb_id"])) +
                                "&sal=" + Funcoes.AESCodifica(Convert.ToString(sal.sal_id)) +
                                "&fil=" + Funcoes.AESCodifica(Convert.ToString(fil.fil_id)) + "'>" +
                                    "<br/><button type='button' class='SalvarExibicaos'>" +
                                        "Editar" +
                                    "</button>" +
                                "</a>" +
                            "</fieldset>";
            }
        }
    }
}