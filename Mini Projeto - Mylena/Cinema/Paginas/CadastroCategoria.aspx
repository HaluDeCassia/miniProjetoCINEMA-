﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CadastroCategoria.aspx.cs" Inherits="Paginas_CadastroCategoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center><fieldset>
        
        <legend>Cadastro de Categorias</legend>
        

    <table>
        <tr>
            <td colspan="2"">
                <asp:Label ID="lblNome" runat="server" Text="Nome: "></asp:Label>
            </td>
            <td colspan="2">    
                <asp:TextBox ID="txbNome" CssClass="tb" runat="server" placeholder="Digite o nome da categoria" ></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnSalvar" CssClass="Salvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click"></asp:Button></td>
            <td><asp:Button ID="btnLimpar" CssClass="Limpar" runat="server" Text="Limpar" OnClick="btnLimpar_Click"></asp:Button></td>
            <td></td>
        </tr>
        </table>
    </fieldset></center>
    
</asp:Content>

