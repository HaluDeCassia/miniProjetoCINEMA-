﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CadastroSala.aspx.cs" Inherits="Paginas_CadastroSala" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><fieldset>
        
        <legend>Cadastro de Salas</legend>
        

    <table>
        <tr>
            <td>
                <asp:Label ID="lblNome" runat="server" Text="Nome: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbNome" CssClass="tb" runat="server" placeholder="Digite o nome da sala" ></asp:TextBox>

            </td>

            <td>
                <asp:Label ID="lblCapacidade" runat="server" Text="Capacidade: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbCapacidade" CssClass="tb" runat="server" placeholder="Digite a capacidade da sala" ></asp:TextBox>

            </td>
            
        </tr>
        <tr>   
            <td colspan="2" style="text-align:right;">
                <asp:Label ID="lblTamanho" runat="server" Text="Tamanho da Tela: "></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="ddlTamanho" runat="server" Width="170px" Height="30px">
                    <asp:ListItem Value="0" Text="Selecionar..."></asp:ListItem>
                    <asp:ListItem Value="1" Text="Grande"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Normal"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnSalvar" CssClass="Salvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click"></asp:Button></td>
            <td><asp:Button ID="btnLimpar" CssClass="Limpar" runat="server" Text="Limpar" OnClick="btnLimpar_Click"></asp:Button></td>
            <td></td>
        </tr>
        </table>
    </fieldset></center>
    <script src="../JS/pnotify.custom.min.js"></script>
    <script>
        function sucess() {
            new PNotify({
                title: 'Parabéns',
                text: 'Cadastrado com Sucesso!',
                type: 'success'
            });
        }
    </script>
    <script>
        function error() {
            new PNotify({
                title: 'Erro',
                text: 'Ocorreu um erro ao cadastrar!',
                type: 'error'
            });
        }
    </script>
</asp:Content>

