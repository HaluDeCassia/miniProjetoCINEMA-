﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_CadastroCategoria : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Categoria cat = new Categoria();
        cat.cat_descricao = txbNome.Text;

        CategoriaDB.Insert(cat);

    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        txbNome.Text = "";
    }
}