﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CadastroFilme.aspx.cs" Inherits="Paginas_CadastroFilme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />

    <center><fieldset>
        
        <legend>Cadastro de Filmes</legend>
        

    <table>
        <tr>
            <td>
                <asp:Label ID="lblNome" runat="server" Text="Nome: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbNome" CssClass="tb" runat="server" placeholder="Digite o nome do filme" ></asp:TextBox>

            </td>

            <td>
                <asp:Label ID="lblSinopse" runat="server" Text="Sinopse: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbSinopse" CssClass="tb" runat="server" placeholder="Digite a sinopse do filme" ></asp:TextBox>

            </td>
            
        </tr>
        <tr>   
            <td>
                <asp:Label ID="lblDuracao" runat="server" Text="Duração: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txbDuracao" CssClass="tb" runat="server" placeholder="Digite a duração do filme" ></asp:TextBox>

            </td>

            <td>
                <asp:Label ID="lblLancamento" runat="server" Text="Ano de Lançamento: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbLancamento" CssClass="tb" runat="server" placeholder="Digite a data de lançamento" ></asp:TextBox>

            </td>
        </tr>
        <tr>   
            <td style="text-align:right">
                <asp:Label ID="lblCategoria" runat="server" Text="Categoria: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCategoria" runat="server" Width="170px" Height="30px"></asp:DropDownList>
            </td>
            <td colspan="3">
                <asp:FileUpload ID="flpArquivos" runat="server" Font-Size="8" AllowMultiple="True" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnSalvar" CssClass="Salvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click"></asp:Button></td>
            <td><asp:Button ID="btnLimpar" CssClass="Limpar" runat="server" Text="Limpar" OnClick="btnLimpar_Click"></asp:Button></td>
            <td></td>
        </tr>
        </table>
        
    </fieldset></center>

    <script src="../JS/pnotify.custom.min.js"></script>
    <script>
        function sucess() {
            new PNotify({
                title: 'Parabéns',
                text: 'Cadastrado com Sucesso!',
                type: 'success'
            });
        }
    </script>
    <script>
        function error() {
            new PNotify({
                title: 'Erro',
                text: 'Ocorreu um erro ao cadastrar!',
                type: 'error'
            });
        }
    </script>

</asp:Content>

