﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Filme
{
    public int fil_id{ get; set; }
    public string fil_nome { get; set; }
    public string fil_duracao{ get; set; }
    public string fil_sinopse { get; set; }
    public string fil_lancamento { get; set; }
    public Categoria fil_categoria { get; set; }
    public string fil_imagem { get; set; }
}