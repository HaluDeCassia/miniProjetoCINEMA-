﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Sala
/// </summary>
public class Sala
{
    public int sal_id { get; set; }
    public string sal_nome { get; set; }
    public string sal_tamanhoTela { get; set; }
    public string sal_capacidade { get; set; }
}