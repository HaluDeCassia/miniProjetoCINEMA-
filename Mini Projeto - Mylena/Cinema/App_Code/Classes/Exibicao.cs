﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Exibicao
/// </summary>
public class Exibicao
{
   public Filme exb_filme { get; set; }
   public Sala exb_sala { get; set;  }
    public string exb_data { get; set; }
    public string exb_horario { get; set; }
    public int exb_id { get; set;  }

}