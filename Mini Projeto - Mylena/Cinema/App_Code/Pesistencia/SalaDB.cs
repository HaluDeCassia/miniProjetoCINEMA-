﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for SalaDB
/// </summary>
public class SalaDB
{
    public static int Insert(Sala sal)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "insert into sal_sala values (0, ?nomeSala, ?tamanhoTela, ?capacidadeSala);";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?nomeSala", sal.sal_nome ));
            objComando.Parameters.Add(Mapped.Parametro("?tamanhoTela", sal.sal_tamanhoTela));
            objComando.Parameters.Add(Mapped.Parametro("?capacidadeSala", sal.sal_capacidade));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }

    public static int Atualizar(Sala sal)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "update sal_sala set sal_nome=?nomeSala, sal_tamanhoTela=?tamanhoTela, sal_capacidade=?capacidadeSala where sal_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?nomeSala", sal.sal_nome));
            objComando.Parameters.Add(Mapped.Parametro("?tamanhoTela", sal.sal_tamanhoTela));
            objComando.Parameters.Add(Mapped.Parametro("?capacidadeSala", sal.sal_capacidade));
            objComando.Parameters.Add(Mapped.Parametro("?id", sal.sal_id));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }


    public static Sala Select(int sal_id)
    {
       Sala sal = null;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            IDataReader objReader;
            objConexao = Mapped.Conexao();

            string sql = "select * from sal_sala where sal_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?id", sal_id));
            objReader = objComando.ExecuteReader();

            while (objReader.Read())
            {
                sal = new Sala();
                sal.sal_id= Convert.ToInt32(objReader["sal_id"]);
                sal.sal_nome= Convert.ToString(objReader["sal_nome"]);
                sal.sal_tamanhoTela = Convert.ToString(objReader["sal_tamanhoTela"]);
                sal.sal_capacidade = Convert.ToString(objReader["sal_capacidade"]);
            }

            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();

            return sal;

        }

        catch (Exception e)
        {
            return sal = null;
        }
    }

    public static DataSet SelectAll()
    {
        DataSet ds = null;

        try
        {
            IDbConnection objConexao = Mapped.Conexao();
            IDbCommand objComando;
            IDataAdapter objAdapter;

            string sql = "select * from sal_sala;";

            objComando = Mapped.Comando(sql, objConexao);
            objAdapter = Mapped.Adapter(objComando);
            ds = new DataSet();
            objAdapter.Fill(ds);
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch (Exception e)
        {
            ds = null;
        }

        return ds;
    }

    public static int Delete(Sala sal)
    {
        int erro = 0;
        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "delete from sal_sala where sal_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?id", sal.sal_id));
            objComando.ExecuteNonQuery();
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch
        {
            erro = -2;
        }

        return erro;
    }
}