﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for CategoriaDB
/// </summary>
public class CategoriaDB
{
   public static int Insert(Categoria cat)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "insert into cat_categoria values (0, ?descricao);";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?descricao", cat.cat_descricao));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }

    public static int Atualizar(Categoria cat)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "update cat_categoria set cat_descricao=?descricao where cat_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?descricao", cat.cat_descricao));
            objComando.Parameters.Add(Mapped.Parametro("?id", cat.cat_id));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }

    public static Categoria Select(int cat_id)
    {
        Categoria cat = null;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            IDataReader objReader;
            objConexao = Mapped.Conexao();

            string sql = "select * from cat_categoria where cat_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?id", cat_id));
            objReader = objComando.ExecuteReader();

            while (objReader.Read())
            {
                cat = new Categoria();
                cat.cat_id = Convert.ToInt32(objReader["cat_id"]);
                cat.cat_descricao = Convert.ToString(objReader["cat_descricao"]);
            }

            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();

            return cat;

        }

        catch (Exception e)
        {
            return cat = null;
        }
    }

    public static DataSet SelectAll()
    {
        DataSet ds = null;

        try
        {
            IDbConnection objConexao = Mapped.Conexao();
            IDbCommand objComando;
            IDataAdapter objAdapter;

            string sql = "select * from cat_categoria;";

            objComando = Mapped.Comando(sql, objConexao);
            objAdapter = Mapped.Adapter(objComando);
            ds = new DataSet();
            objAdapter.Fill(ds);
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch (Exception e)
        {
            ds = null;
        }

        return ds;
    }

    public static int Delete(Categoria cat)
    {
        int erro = 0;
        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "delete from cat_categoria where cat_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?codigo", cat.cat_id));
            objComando.ExecuteNonQuery();
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch
        {
            erro = -2;
        }

        return erro;
    }
}